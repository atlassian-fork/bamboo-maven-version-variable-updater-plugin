# Variable tasks for Bamboo

A few Bamboo tasks to help managing releases from Bamboo. User documentation can be found in the [wiki](https://bitbucket.org/atlassian/bamboo-maven-version-variable-updater-plugin/wiki/Home).

## Building and Developing

To build it:

    mvn3 install

To run it, use [AMPS](https://developer.atlassian.com/display/DOCS/Getting+Started):

    atlas-debug


 CI Build is on [SBAC](https://staging-bamboo.internal.atlassian.com/browse/BAMBOOPLUGINSV2-BAMBOOVARIABLEUPDATER)