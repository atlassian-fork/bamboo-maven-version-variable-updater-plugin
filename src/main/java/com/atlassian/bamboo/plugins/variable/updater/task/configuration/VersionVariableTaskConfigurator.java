package com.atlassian.bamboo.plugins.variable.updater.task.configuration;


import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.variable.updater.VariableUpdater;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import static com.atlassian.bamboo.plugins.variable.updater.task.executor.VersionVariableTask.INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY;
import static com.atlassian.bamboo.plugins.variable.updater.task.executor.VersionVariableTask.KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY;
import static com.atlassian.bamboo.plugins.variable.updater.task.executor.VersionVariableTask.OVERRIDE_BRANCH_VARIABLE;
import static com.atlassian.bamboo.plugins.variable.updater.task.executor.VersionVariableTask.SCOPE_VARIABLE_CONFIG_KEY;
import static com.atlassian.bamboo.plugins.variable.updater.task.executor.VersionVariableTask.VARIABLE_CONFIG_KEY;

public abstract class VersionVariableTaskConfigurator extends AbstractTaskConfigurator {
    void populateLists(@NotNull final Map<String, Object> context) {
        final ImmutableMap<String, String> variableScopeOptions =
                new ImmutableMap.Builder<String, String>()
                        .put(VariableUpdater.SCOPE.JOB.name(), getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.scope.job"))
                        .put(VariableUpdater.SCOPE.RESULT.name(), getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.scope.result"))
                        .put(VariableUpdater.SCOPE.PLAN.name(), getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.scope.plan"))
                        .build();

        context.put("variableScopeOptions", variableScopeOptions);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(VARIABLE_CONFIG_KEY, params.getString(VARIABLE_CONFIG_KEY));
        config.put(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY, params.getString(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));
        config.put(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY, params.getString(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY));
        config.put(SCOPE_VARIABLE_CONFIG_KEY, params.getString(SCOPE_VARIABLE_CONFIG_KEY));
        config.put(OVERRIDE_BRANCH_VARIABLE, params.getString(OVERRIDE_BRANCH_VARIABLE));

        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        context.put(VARIABLE_CONFIG_KEY, "");
        context.put(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY, false);
        context.put(OVERRIDE_BRANCH_VARIABLE, false);
        context.put(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY, false);
        context.put(SCOPE_VARIABLE_CONFIG_KEY, VariableUpdater.SCOPE.JOB);

        populateLists(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put(VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(VARIABLE_CONFIG_KEY));
        context.put(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));
        context.put(OVERRIDE_BRANCH_VARIABLE, taskDefinition.getConfiguration().get(OVERRIDE_BRANCH_VARIABLE));
        context.put(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY));
        context.put(SCOPE_VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(SCOPE_VARIABLE_CONFIG_KEY));
        populateLists(context);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put(VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(VARIABLE_CONFIG_KEY));
        context.put(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));
        context.put(OVERRIDE_BRANCH_VARIABLE, taskDefinition.getConfiguration().get(OVERRIDE_BRANCH_VARIABLE));
        context.put(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY));
        context.put(SCOPE_VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(SCOPE_VARIABLE_CONFIG_KEY));
    }
    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        if (params.getString(VARIABLE_CONFIG_KEY) == null || params.getString(VARIABLE_CONFIG_KEY).isEmpty()){
            errorCollection.addError(VARIABLE_CONFIG_KEY, getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.empty.error"));
        }
    }
}
