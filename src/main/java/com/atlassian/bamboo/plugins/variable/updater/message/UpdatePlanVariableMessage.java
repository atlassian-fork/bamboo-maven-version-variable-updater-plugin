package com.atlassian.bamboo.plugins.variable.updater.message;

import com.atlassian.bamboo.ResultKey;
import com.atlassian.bamboo.plugins.variable.updater.VariableUpdater;
import com.atlassian.bamboo.v2.build.agent.messages.BambooAgentMessage;
import org.jetbrains.annotations.Nullable;

public class UpdatePlanVariableMessage implements BambooAgentMessage
{
    private final ResultKey resultKey;
    private final String planKeyOrEnvironmentId;
    private final String variableName;
    private final String newValue;
    private final boolean includeGlobals;
    private final boolean useBranchVariables;

    public UpdatePlanVariableMessage(ResultKey resultKey, String planKeyOrEnvironmentId, String variableName, String newValue, boolean includeGlobals, boolean useBranchVariables)
    {
        this.resultKey = resultKey;
        this.planKeyOrEnvironmentId = planKeyOrEnvironmentId;
        this.variableName = variableName;
        this.newValue = newValue;
        this.includeGlobals = includeGlobals;
        this.useBranchVariables = useBranchVariables;
    }

    @Nullable
    @Override
    public Object deliver()
    {
        VariableUpdater.savePlanVariableServerSide(resultKey, planKeyOrEnvironmentId, variableName, newValue, includeGlobals, useBranchVariables);
        return null;
    }


}
