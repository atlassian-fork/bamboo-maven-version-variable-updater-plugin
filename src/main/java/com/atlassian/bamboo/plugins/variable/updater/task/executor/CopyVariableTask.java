package com.atlassian.bamboo.plugins.variable.updater.task.executor;

import java.util.Map;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.ResultKey;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.plugins.variable.updater.VariableUpdater;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.variable.VariableContext;

public class CopyVariableTask extends VersionVariableTask
{
    public static final String ORIGIN_VARIABLE_CONFIG = "originVariable";

    @NotNull
    @Override
    public TaskResult executeTask(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final String variableName = taskContext.getConfigurationMap().get(VARIABLE_CONFIG_KEY);
        final VariableUpdater.SCOPE variableScope = VariableUpdater.SCOPE.valueOf(taskContext.getConfigurationMap().get(SCOPE_VARIABLE_CONFIG_KEY).toUpperCase());
        final boolean includeGlobals = Boolean.parseBoolean(taskContext.getConfigurationMap().get(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));
        final boolean useBranchVariables = Boolean.parseBoolean(taskContext.getConfigurationMap().get(OVERRIDE_BRANCH_VARIABLE));
        final String originVariableName = taskContext.getConfigurationMap().get(ORIGIN_VARIABLE_CONFIG);
        final boolean keepCustomised = Boolean.parseBoolean(taskContext.getConfigurationMap().get(KEEP_CUSTOMISED_VARIABLE_CONFIG_KEY));

        final BuildLogger buildLogger = taskContext.getBuildLogger();

        final String planKeyOrEnvironmentId;
        final ResultKey resultKey;
        if (taskContext instanceof DeploymentTaskContext){
            final DeploymentTaskContext deploymentTaskContext = (DeploymentTaskContext) taskContext;
            final long deploymentEnvironmentId = deploymentTaskContext.getDeploymentContext().getEnvironmentId();
            planKeyOrEnvironmentId = String.valueOf(deploymentEnvironmentId);
            resultKey = deploymentTaskContext.getDeploymentContext().getResultKey();

        } else if (taskContext instanceof TaskContext){
            final TaskContext tContext = (TaskContext) taskContext;
            planKeyOrEnvironmentId = tContext.getBuildContext().getParentBuildContext().getPlanKey();
            resultKey = tContext.getBuildContext().getResultKey();
        } else {
            throw new TaskException("TaskContext not defined: " + taskContext.getClass().getName());
        }
        final Map<String, String> buildCustomVariables = taskContext.getCommonContext().getCurrentResult()
                .getCustomBuildData();
        final VariableContext variableContext = taskContext.getCommonContext().getVariableContext();
        final VariableUpdater.SAVE_STRATEGY strategy = retrieveStrategy(variableName, keepCustomised, variableContext);
        final String currentValue;
        if (strategy == VariableUpdater.SAVE_STRATEGY.SKIP){
            return skipUpdatingIfConflictWithCustomVariables(taskContext, variableName);
        } else {
            currentValue = retrieveCurrentValue(originVariableName, buildCustomVariables, variableContext, taskContext.getBuildLogger());
        }

        final VariableUpdater updater = new VariableUpdater(resultKey, taskContext.getWorkingDirectory().getAbsolutePath(), buildCustomVariables);
        updater.update(planKeyOrEnvironmentId, variableName, currentValue,includeGlobals, variableScope,
                isRemote(), buildLogger, variableContext, useBranchVariables);

        return TaskResultBuilder.newBuilder(taskContext).success().build();
    }


}